﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameCounter
{
    internal class PluginInfos
    {
        public const string PLUGIN_ID = "com.gitlab.axolotlll.llb-framecounter";
        public const string PLUGIN_NAME = "FrameCounter";
        public const string PLUGIN_VERSION = "1.0.0";
    }
}
