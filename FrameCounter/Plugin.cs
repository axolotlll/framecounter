﻿using Abilities;
using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using LLBML.Utils;
using Multiplayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace FrameCounter
{
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    public class Plugin : BaseUnityPlugin
    {
        static public bool InGame => World.instance != null && (DNPFJHMAIBP.HHMOGKIMBNM() == JOFJHDJHJGI.CDOFDJMLGLO || DNPFJHMAIBP.HHMOGKIMBNM() == JOFJHDJHJGI.LGILIJKMKOD) && !LLScreen.UIScreen.loadingScreenActive;
        static public JOFJHDJHJGI CurrentGameState => DNPFJHMAIBP.HHMOGKIMBNM();
        static public GameMode CurrentGameMode => JOMBNFKIHIC.GIGAKBJGFDI.PNJOKAICMNN;

        static Queue<string> actionQueue;

        Rect actionRect;

        const int width = 105;

        static int LastAction = 0;

        static GUIStyle style
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle(GUI.skin.label)
                {
                    fontSize = 15,
                    alignment = TextAnchor.MiddleLeft,
                };
                gUIStyle.normal.background = ColorToTexture2D(new Color(0, 0, 0, 0.1f));
                gUIStyle.normal.textColor = Color.yellow;
                return gUIStyle;
            }

        }

        static int height
        {
            get
            {
                return 10 + actionQueue.Count * 30;
            }
        }

        static ConfigEntry<bool> Enabled;
        static ConfigEntry<int> NumActions;
        static ConfigEntry<int> XPos;
        static ConfigEntry<int> YPos;

        void Awake()
        {
            var harm = new Harmony(PluginInfos.PLUGIN_ID);
            harm.PatchAll();

            InitConfig();
        }
        void Start()
        {
            actionQueue = new Queue<string>();
            actionRect = new Rect( (Screen.width - width - 20), Screen.height - height - 20, width, height);

            ModDependenciesUtils.RegisterToModMenu(this.Info);
        }

        void InitConfig()
        {
            Enabled = Config.Bind<bool>("Toggles", "enabled", true, new ConfigDescription("Enabled"));
            NumActions = Config.Bind<int>("Tuning", "NumActions", 4, new ConfigDescription("Number of Actions to Display", new AcceptableValueRange<int>(0, 10)));
            XPos = Config.Bind<int>("Tuning", "offsetX", 1100, new ConfigDescription("X Position of Box", new AcceptableValueRange<int>(0, 1920)));
            YPos = Config.Bind<int>("Tuning", "offsetY", 420, new ConfigDescription("Y Position of Box", new AcceptableValueRange<int>(0, 1080)));
        }
        static Texture2D ColorToTexture2D(Color32 color)
        {
            Texture2D texture2D = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            texture2D.SetPixel(0, 0, color);
            texture2D.Apply();
            return texture2D;
        }


        void OnGUI()
        {
            if (!Enabled.Value) return;
            if (CurrentGameMode != GameMode.TRAINING) return;
            if (!InGame) return;
            if (actionQueue.Count < 1) return;
            actionRect = new Rect(XPos.Value, YPos.Value, width, height);
            GUILayout.BeginArea(actionRect);
            GUILayout.FlexibleSpace();

            for (int i = 0; i < actionQueue.Count; i++)
            {
                GUILayout.Label(actionQueue.ElementAt(i), style);
            }

            GUILayout.EndArea();
        }

        static public void NewAction(string type)
        {
            if (!Enabled.Value) return;
            int diff = OGONAGCFDPK.DDBJKEIHELD - LastAction;
            
            actionQueue.Enqueue(String.Format("{0} +({1})f", type, diff));
            if (actionQueue.Count > NumActions.Value)
            {
                actionQueue.Dequeue();
            }

            LastAction = OGONAGCFDPK.DDBJKEIHELD;
        }
    }

    [HarmonyPatch(typeof(BuntAbility), nameof(BuntAbility.SetAbilityState))]
    public class BuntPatch
    {
        public static void Postfix(string state)
        {
            if (state == "PRE_BUNT") Plugin.NewAction("Bunt");
        }
    }

    [HarmonyPatch(typeof(SmashAbility), nameof(SmashAbility.SetAbilityState))]
    public class SmashPatch
    {
        public static void Postfix(string state)
        {
            if (state == "SMASH_WIND_UP") Plugin.NewAction("Smash");
        }
    }

    [HarmonyPatch(typeof(NeutralSwingAbility), nameof(NeutralSwingAbility.SetAbilityState))]
    public class SwingPatch
    {
        public static void Postfix(string state)
        {
            if (state == "SWING_PRE_WIND_UP") Plugin.NewAction("Swing");
        }
    }

    [HarmonyPatch(typeof(GrabAbility), nameof(GrabAbility.SetAbilityState))]
    public class GrabPatch
    {
        public static void Postfix(string state)
        {
            if (state == "IN_GRAB_NEW") Plugin.NewAction("Grab");
        }
    }
}
